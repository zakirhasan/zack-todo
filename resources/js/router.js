import Vue from 'vue'
import VueRouter from "vue-router";
import {store} from "./store/store";

Vue.use(VueRouter)

import Home from "./components/Home";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import TodoList from "./components/todo/TodoList";
import UserProfile from "./components/auth/UserProfile";
import ChangePassword from "./components/auth/ChangePassword";


export const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: Home },
        { path: '/login', component: Login },
        { path: '/register', component: Register },
        { path: '/todos', component: TodoList },
        { path: '/profile', component: UserProfile },
        { path: '/change-password', component: ChangePassword }
    ]
})

router.beforeEach((to, from, next) => {
    store.dispatch('initAuth')
    if(to.path == "/login" || to.path == "/register") {
        if(store.getters.isAuthenticated){
            next('/')
        }else {
            next()
        }
    } else if(to.path == "/todos" || to.path == "/profile" || to.path == "/change-password"){
        if(store.getters.isAuthenticated){
            next()
        }else {
            next('/login');
        }
    } else {
        next();
    }
})
