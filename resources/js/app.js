import Vue from 'vue'
import { router } from "./router";
import { store } from "./store/store";
import App from './App.vue'
import Vuelidate from "vuelidate/src";

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.baseURL = 'api/';

Vue.use(Vuelidate)

require('animate.css')



const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})
