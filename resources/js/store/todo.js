const state = {
    todoList: []
}

const getters = {
    todoList(state){
        return state.todoList
    }
}

const mutations = {
    todoList(state, payload){
        state.todoList = payload
    }
}

const actions = {
    initTodoList(vuexContext){
        vuexContext.commit('loading',true)
        return axios.post('todos/index', {userId: vuexContext.getters.userId})
            .then(res => {
                vuexContext.commit('todoList', res.data)
                vuexContext.commit('loading', false)
            })
    },
    addTodo(vuexContext, payload){
        vuexContext.commit('loading',true)
        return axios.post('todos/store', { ...payload, userId: vuexContext.getters.userId })
            .then(res => {
                if(res.data.success){
                    vuexContext.dispatch('initTodoList')
                }
            })
    },
    updateTodo(vuexContext, payload){
        vuexContext.commit('loading',true)
        return axios.put('todos/store', { ...payload, userId: vuexContext.getters.userId })
            .then(res => {
                if(res.data.success){
                    vuexContext.dispatch('initTodoList')
                }
            })
    },
    deleteTodo(vuexContext, payload){
        vuexContext.commit('loading',true)
        return axios.delete('todos/'+payload)
            .then(res => {
                if(res.data.success){
                    vuexContext.dispatch('initTodoList')
                }
            })
    }

}

export default {
    state,
    getters,
    mutations,
    actions
}
