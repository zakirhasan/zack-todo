import Vue from 'vue'
import Vuex from 'vuex'
import auth from "./auth";
import todo from "./todo";

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        loading: false
    },
    getters: {
        loading(state){
            return state.loading
        }
    },
    mutations: {
        loading(state, payload){
            state.loading = payload
        }
    },
    actions: {

    },
    modules: {
        auth,
        todo
    }
})
