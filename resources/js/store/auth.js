const state = {
    idToken: null,
    expiresIn: null,
    user: {}
}
const getters = {
    isAuthenticated(state){
        return state.idToken != null && state.expiresIn != null;
    },
    getUser(state){
        return state.user
    },
    userId(state){
        return state.user.id
    }
}
const mutations = {
    login(state, payload){
        state.idToken = payload.idToken
        state.expiresIn = payload.expiresIn
        state.user = payload.user

        window.axios.defaults.headers.common['Authorization'] = 'Bearer '+payload.idToken;

        localStorage.setItem('idToken', payload.idToken)
        localStorage.setItem('expiresIn', payload.expiresIn)
        localStorage.setItem('user', JSON.stringify(payload.user))
    },
    logout(state, payload) {
        state.idToken = null
        state.expiresIn = null
        state.user = null

        window.axios.defaults.headers.common['Authorization'] = null;

        localStorage.removeItem('idToken')
        localStorage.removeItem('expiresIn')
        localStorage.removeItem('user')
    },
    updateUser(state, payload){
        state.user = payload.user

        localStorage.setItem('user', JSON.stringify(payload.user))
    }
}
const actions = {
    register(vuexContext, payload) {
        vuexContext.commit('loading', true);
        return new Promise((resolve, reject) => {
            axios.post('auth/register', payload)
                .then(res => {
                    let data = res.data

                    if(data.error){
                        resolve({
                            error: true,
                            message: data.error
                        })
                    }else {
                        vuexContext.commit('login', {
                            idToken: data.idToken,
                            expiresIn: new Date().getTime() + (data.expiresIn * 1000),
                            user: data.user
                        })
                        resolve({
                            success: true
                        })
                    }
                    vuexContext.commit('loading', false)
                })
                .catch(err => {
                    reject({error: err})
                })
        })
    },
    login(vuexContext, payload) {
        vuexContext.commit('loading', true);
        return new Promise((resolve, reject) => {
            axios.post('auth/login', payload)
                .then(res => {
                    let data = res.data
                    if(data.error){
                        resolve({
                            error: true,
                            message: data.error
                        })
                    }else {
                        vuexContext.commit('login', {
                            idToken: data.idToken,
                            expiresIn: new Date().getTime() + (data.expiresIn * 1000),
                            user: data.user
                        })
                        resolve({
                            error: false
                        })
                    }
                    vuexContext.commit('loading', false)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    logout(vuexContext, payload) {
        vuexContext.commit('loading', true);
        return axios.post('auth/logout')
            .then(res => {
                vuexContext.commit('logout')
                vuexContext.commit('loading', false);
            })
            .catch(err => {
                console.error(err);
            })
    },
    initAuth(vuexContext) {
        let idToken = localStorage.getItem('idToken');
        let expiresIn = localStorage.getItem('expiresIn');
        let user = JSON.parse(localStorage.getItem('user'));

        if(idToken && expiresIn && user) {
            if(new Date().getTime() > expiresIn){
                vuexContext.commit('logout')
            }else {
                vuexContext.commit('login', {
                    idToken, user, expiresIn
                })
            }

        }
    },
    updateProfile(vuexContext, payload) {
        vuexContext.commit('loading', true);
        return axios.post('auth/update-profile', {...payload, id: vuexContext.getters.userId})
            .then(res => {
                vuexContext.dispatch('logout');
                vuexContext.commit('loading', false);
            })
    }

}

export default {
    state,
    getters,
    mutations,
    actions
}
