<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {

    Route::post('register', 'AuthController@register');
    Route::post('update-profile', 'AuthController@updateProfile');
    Route::post('email-control', 'AuthController@emailControl');
    Route::post('password-control', 'AuthController@passwordControl');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group([
    'middleware' => 'jwt.auth',
], function ($router) {

    Route::group(['prefix' => 'todos'], function ($router) {
        Route::post('/index', 'TodoController@index');
        Route::post('/store', 'TodoController@store');
        Route::put('/store', 'TodoController@store');
        Route::delete('{id}', 'TodoController@destroy');
    });
});
