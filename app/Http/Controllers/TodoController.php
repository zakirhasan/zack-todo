<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public  function index(Request $request)
    {
        $todos = Todo::where('userId','=',$request->input('userId'))->get();

        return response()->json($todos);
    }

    public function store(Request $request)
    {
        $todo = $request->isMethod('put') ? Todo::findOrFail($request->input('id')) : new Todo;

        $todo->title = $request->input('title');
        $todo->status = $request->input('status');
        $todo->userId = $request->input('userId');

        if($todo->save()){
            return response()->json(['success' => true], 200);
        }else {
            return response()->json(['error' => true], 401);
        }
    }

    public function destroy($id)
    {

        $todo = Todo::findOrFail($id);

        if($todo->delete()){
            return response()->json(['success' => true], 200);
        }else {
            return response()->json(['error' => true], 401);
        }
    }
}
