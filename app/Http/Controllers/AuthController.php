<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register', 'emailControl']]);
    }

    public function passwordControl(Request $request)
    {
        $user = User::find($request->input('id'));

        if(Hash::check($request->input('password'), $user->password)){
            return response()->json(true);
        }else {
            return response()->json(false);
        }

    }


    public function updateProfile(Request $request)
    {
        $user = User::findOrFail($request->input('id'));

        $user->name = $request->input('name') ? $request->input('name') : $user->name;
        $user->email = $request->input('email') ? $request->input('email') : $user->email;
        $user->password = $request->input('password') ? bcrypt($request->input('password')) : $user->password;

        if($user->save()){
            return response()->json(['user'=>$user], 200);
        }else {
            return response()->json(['error'=>'Bir Sorun Oluştu'], 401);
        }
    }

    public function register(Request $request)
    {

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));

        if($user->save()){
            $credentials = request(['email', 'password']);

            $token = auth('api')->attempt($credentials);

            return $this->respondWithToken($token);
        }else {
            return response()->json(['error'=>'Bir Sorun Oluştu'], 401);
        }
    }

    public function emailControl(Request $request)
    {
        $count = count(User::where('email','=',$request->input('email'))->get());

        return $count;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Kullanıcı Adı veya Şifre Yanlış'], 200);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Başarıyla Çıkış Yapıldı']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'idToken' => $token,
            'user' => $this->guard()->user(),
            'tokenType' => 'bearer',
            'expiresIn' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    public function guard() {
        return \Auth::Guard('api');
    }
}
